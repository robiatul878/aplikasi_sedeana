import {Stylesheet, Text, View, TouchableOpacity, TextInput} from 'react-native';
import React, {Component} from 'react';

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      warnaText: 'blue',
      inputPertama: '',
      inputKedua: '',
      fullText: '',
    };
  }

  ubahwarna = () => {
    const warna = this.state.warnaText;
    if (warna === 'blue'){
      this.setState({warnaText: 'green'});
    } else if (warna === 'green') {
      this.setState({warnaText: 'red'});
    } else {
      this.setState({warnaText: 'blue'});
    }
  };

  render () {
    return (
      <View> 
      <Text style={{color: this.state.warnaText}}> Halo React Native </Text>
      <TextInput></TextInput>
      <TextInput></TextInput>
      <Text>ful Text: {this.state.fullText }</Text>
      <TouchableOpacity onPress={() => this.ubahwarna()}>
        <Text>Klik</Text>
      </TouchableOpacity>
      </View>
    );
  }
}


export default App;

// const styles = Stylesheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center', 
//     alignItem: 'center',
//   },
// });

